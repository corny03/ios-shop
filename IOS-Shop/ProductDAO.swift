//
//  ProductDAO.swift
//  IOS-Shop
//
//  Created by Berry Bonton on 11/07/2017.
//  Copyright © 2017 UJ. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class ProductDAO {
    
    var context: NSManagedObjectContext? = nil
    
    init(appcontext: NSManagedObjectContext) {
        context = appcontext
    }
    
    func getProducts() -> [NSManagedObject] {
        
        var products:[NSManagedObject] = []
        
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Product")
        
        do {
            products = try context!.fetch(fetchRequest)
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        
        return products;
    }
    
    func fetchProducts(){
        let json = JSON()
        json.fetchProductJSON{
            products in
            for product in products {
                let productDict = product as! NSDictionary
                let title = productDict.value(forKey: "title")
                let id = productDict.value(forKey: "id") as! String
                
                if(!self.existsProduct(id: id)){
                    
                    print("Adding product: \(String(describing: title)) into db")
                    
                    let newProduct = Product(context: self.context!)
                    newProduct.id = Int32(productDict.value(forKey: "id") as! String)!
                    newProduct.title = productDict.value(forKey: "title") as! String?
                    newProduct.desc = productDict.value(forKey: "desc") as! String?
                    newProduct.imgUrl = productDict.value(forKey: "imgUrl") as! String?
                    newProduct.categoryId = Int32(productDict.value(forKey: "categoryId") as! String)!
                    newProduct.price = productDict.value(forKey: "price") as! String?
                    
                    self.addProduct(newProduct: newProduct)
                    
                } else {
                    print("Product \(String(describing: title)) already exists")
                }
                
                
            }
        }
    }
    
    func existsProduct(id: String) -> Bool {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Product")
        request.predicate = NSPredicate(format: "id=%@", id)
        do {
            let count = try self.context?.count(for: request)
            if count! > 0 {return true}
        } catch {
            print("Blad.")
        }
        return false
    }
    
    func addProduct(newProduct: Product) {
        do {
            context?.insert(newProduct)
            try context?.save()
        } catch {
            fatalError("Failure to save context: \(error)")
        }
    }
    
    func getProductsByCategoryId(categoryId: String) -> [NSManagedObject] {
        var products:[NSManagedObject] = []
        let request = NSFetchRequest<NSManagedObject>(entityName: "Product")
        
        request.predicate = NSPredicate(format: "categoryId=%@", categoryId)
        
        do {
            products = try context!.fetch(request)
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        
        return products;
    }
    
}
