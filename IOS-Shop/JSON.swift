//
//  JSON.swift
//  IOS-Shop
//
//  Created by Berry Bonton on 02/07/2017.
//  Copyright © 2017 UJ. All rights reserved.
//

import Foundation

class JSON {
    
    let categoriesURL = "http://localhost:8080/categories"
    
    
    func fetchCategoryJSON(completionHandler: @escaping (NSArray)->Void) {
        let url = URL(string: categoriesURL)
        var elements: NSArray = []
        
        let urlSession = URLSession.shared
        
        let task = urlSession.dataTask(with: url!) { (data, response, error) in
            if error != nil {
                completionHandler(elements)
                return
            }
            
            do {
                elements = try JSONSerialization.jsonObject(with: data! as Data, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSArray
                completionHandler(elements)
            } catch {
                print("nie bangla")
            }
        }
        task.resume()
    }
    
    func fetchProductJSON(completionHandler: @escaping (NSArray)->Void) {
        let productsURL = "http://localhost:8080/products"
        
        let url = URL(string: productsURL)
        var elements: NSArray = []
        
        let urlSession = URLSession.shared
        
        let task = urlSession.dataTask(with: url!) { (data, response, error) in
            if error != nil {
                completionHandler(elements)
                return
            }
            
            do {
                elements = try JSONSerialization.jsonObject(with: data! as Data, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSArray
                completionHandler(elements)
            } catch {
                print("nie bangla")
            }
        }
        task.resume()
    }
    func fetchJSON(url: URL, completionHandler: @escaping (NSArray)->Void) {
        let url = URL(string: categoriesURL)
        var elements: NSArray = []
        
        let urlSession = URLSession.shared
        
        let task = urlSession.dataTask(with: url!) { (data, response, error) in
            if error != nil {
                completionHandler(elements)
                return
            }
            
            do {
                elements = try JSONSerialization.jsonObject(with: data! as Data, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSArray
                completionHandler(elements)
            } catch {
                print("nie bangla")
            }
        }
        task.resume()
    }
    
    func parseJSON(inputData: NSData) -> NSDictionary{
        print(inputData)
        var dictionary: NSDictionary = [:]
        do {
            dictionary = try JSONSerialization.jsonObject(with: inputData as Data, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
            print(dictionary)
            return dictionary
        } catch {
            print("Not able to load")
        }
        return dictionary
    }
}
