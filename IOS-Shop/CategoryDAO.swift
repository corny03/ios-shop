//
//  CategoryDAO.swift
//  IOS-Shop
//
//  Created by Berry Bonton on 02/07/2017.
//  Copyright © 2017 UJ. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class CategoryDAO {
    
    var context: NSManagedObjectContext? = nil
    
    init(appcontext: NSManagedObjectContext) {
        context = appcontext
    }
    
    func getCategories() -> [NSManagedObject] {
        
        var categories:[NSManagedObject] = []
        
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Category")
        
        do {
            categories = try context!.fetch(fetchRequest)
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        
        return categories;
    }
    
    func fetchCategories(){
        let json = JSON()
        json.fetchCategoryJSON{
            categories in
            for category in categories {
                let categoryDict = category as! NSDictionary
                let name = categoryDict.value(forKey: "name")
                let id = categoryDict.value(forKey: "id") as! String
                
                if(!self.existsCategory(id: id)){
                    
                    print("Adding category: \(String(describing: name)) into db")
                    
                    let newCategory = Category(context: self.context!)
                    newCategory.id = Int32(categoryDict.value(forKey: "id") as! String)!
                    newCategory.name = categoryDict.value(forKey: "name") as! String?
                    newCategory.desc = categoryDict.value(forKey: "desc") as! String?
                    
                    self.addCategory(newCategory: newCategory)
                    
                } else {
                    print("Category \(String(describing: name)) already exists")
                }
                
                
            }
        }
    }
    
    func existsCategory(id: String) -> Bool {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Category")
        request.predicate = NSPredicate(format: "id=%@", id)
        do {
            let count = try self.context?.count(for: request)
            if count! > 0 {return true}
        } catch {
            print("Blad.")
        }
        return false
    }
    
    func addCategory(newCategory: Category) {
        do {
            context?.insert(newCategory)
            try context?.save()
        } catch {
            fatalError("Failure to save context: \(error)")
        }
    }
    
}
