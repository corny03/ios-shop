//
//  CategoriesTableViewCell.swift
//  IOS-Shop
//
//  Created by Berry Bonton on 11/07/2017.
//  Copyright © 2017 UJ. All rights reserved.
//

import UIKit

class CategoriesTableViewCell: UITableViewCell {
    
    //MARK: aaa
    //@IBOutlet weak var categoryDesc: UILabel!
    @IBOutlet weak var categoryTitle: UILabel!
    @IBOutlet weak var categoryDesc: UITextView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
