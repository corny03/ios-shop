//
//  ProductDetailsViewController.swift
//  IOS-Shop
//
//  Created by Berry Bonton on 15/07/2017.
//  Copyright © 2017 UJ. All rights reserved.
//

import UIKit

class ProductDetailsViewController: UIViewController {
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var productDescTextView: UITextView!
    //@IBOutlet weak var productPriceLabel: UITextField!
    @IBOutlet weak var productPriceLabel: UILabel!

    var selectedProductCell : ProductTableViewCell?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.productNameLabel.text = selectedProductCell?.productNameLabel.text
        self.productDescTextView.text = selectedProductCell?.productDescTextView.text
        self.productPriceLabel.text = selectedProductCell?.productPriceLabel.text
        self.productImage.image = selectedProductCell?.productImageView.image
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
