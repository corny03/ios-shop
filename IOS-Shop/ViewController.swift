//
//  ViewController.swift
//  IOS-Shop
//
//  Created by Berry Bonton on 02/07/2017.
//  Copyright © 2017 UJ. All rights reserved.
//

import UIKit
import CoreData


class ViewController: UIViewController {
    
    var category: [NSManagedObject] = []
    var products: [NSManagedObject] = []
    var productDAO : ProductDAO!
    
    @IBOutlet weak var categoriesTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        title = "Kategorie"
        categoriesTableView.register(UITableViewCell.self,
                           forCellReuseIdentifier: "Cell")
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.fetchCategories()
        self.reloadCategories()
        self.categoriesTableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }

    @IBAction func refreshCategories(_ sender: UIBarButtonItem) {
        print("Odswiezam...")
        
        self.fetchCategories()
        self.reloadCategories()
        self.categoriesTableView.reloadData()
        
        print("Odswiezono.")
    }
    
    func fetchCategories(){
        print("Pobieram kategorie...")
        
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        let categoryDAO = CategoryDAO(appcontext: (appDelegate?.persistentContainer.viewContext)!)
        categoryDAO.fetchCategories()
        productDAO = ProductDAO(appcontext: (appDelegate?.persistentContainer.viewContext)!)
        productDAO.fetchProducts()
        usleep(250000)
        
        print("Pobrano.")
    }
 
    
    func reloadCategories(){
        print("Przeladowuje kategorie...")
        
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        let categoryDAO = CategoryDAO(appcontext: (appDelegate?.persistentContainer.viewContext)!)
        category = categoryDAO.getCategories()
        
        print("Przeladowano.")
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        guard let productTableViewController = segue.destination as? ProductTableViewController else{
            fatalError("Nie udało się stworzyc")
        }
        
        guard let selectedCategoryCell = sender as? CategoriesTableViewCell else {
            fatalError("Unexpected sender")
        }
        
        guard let indexPath = categoriesTableView.indexPath(for: selectedCategoryCell) else {
            fatalError("The selected cell is not being displayed by the table")
        }
        let selectedItem: NSManagedObject = category[indexPath.row] as NSManagedObject

        let selectedCategoryId =  String(describing: selectedItem.value(forKey: "id") as! Int)
        
        let products = productDAO.getProductsByCategoryId(categoryId: selectedCategoryId)
        print("Mam produktów -> " + String(products.count))
        productTableViewController.products = products
        
    }
    
}

extension ViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int {
        return category.count
    }
    
    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath)
        -> UITableViewCell {
            let cellIdentifier = "CategoriesTableViewCell"
            guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier,
                                                           for: indexPath) as? CategoriesTableViewCell else {
                                                            fatalError("Nie bangla")
            }
            let cat = category[indexPath.row]
            
            cell.categoryTitle.text = cat.value(forKeyPath: "name") as? String
            cell.categoryDesc.text = cat.value(forKeyPath: "desc") as? String
            
            return cell
    }
}

