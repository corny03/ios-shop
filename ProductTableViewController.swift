//
//  ProductTableViewController.swift
//  IOS-Shop
//
//  Created by Berry Bonton on 13/07/2017.
//  Copyright © 2017 UJ. All rights reserved.
//

import UIKit
import CoreData

class ProductTableViewController: UITableViewController {
    var products: [NSManagedObject] = []
    let serverAssetsURL = "http://localhost:8080"
    @IBOutlet weak var productsTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return products.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Table view cells are reused and should be dequeued using a cell identifier.
        let cellIdentifier = "ProductTableViewCell"
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? ProductTableViewCell  else {
            fatalError("The dequeued cell is not an instance of ProductTableViewCell.")
        }
        
        // Fetches the appropriate meal for the data source layout.
        let product = products[indexPath.row]
        
        cell.productNameLabel.text = product.value(forKeyPath: "title") as? String
        
        var imageData: NSData
        
        do{
            let imgSrc = product.value(forKey: "imgUrl") as! String
            print(serverAssetsURL + imgSrc)
            let url = URL(string: serverAssetsURL + imgSrc)
            imageData = try NSData(contentsOf: url!)
            cell.productImageView.image = UIImage(data: imageData as Data)
        } catch{
          cell.productImageView.image = #imageLiteral(resourceName: "defaultphoto")
        }
        cell.productPriceLabel.text = product.value(forKey: "price") as? String
        cell.productDescTextView.text = product.value(forKey: "desc") as? String
        
        return cell
    }


    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print("Ale jaja")
        super.prepare(for: segue, sender: sender)
        
        guard let productDetailsViewController = segue.destination as? ProductDetailsViewController else{
            fatalError("Nie udało się stworzyc")
        }
        
        guard let selectedProductCell = sender as? ProductTableViewCell else {
            fatalError("Unexpected sender")
        }
//        
        guard let indexPath = productsTableView.indexPath(for: selectedProductCell) else {
                        fatalError("The selected cell is not being displayed by the table")
        }
        // let selectedProduct: NSManagedObject = products[indexPath.row] as NSManaged
        productDetailsViewController.selectedProductCell = selectedProductCell

        
    }
    

}
